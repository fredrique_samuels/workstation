/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even t he implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.workstation;

import org.apache.log4j.BasicConfigurator;

import com.iv3d.common.Provider;

import iv3d.ui.common.UiTaskResult;
import iv3d.workstation.common.ObjectProviderAdapter;
import iv3d.workstation.jython.JythonUtils;
import iv3d.workstation.jython.JythonUtils.JythonException;
import iv3d.workstation.paths.PathManager;
import iv3d.workstation.paths.domain.DefaultPathManager;
import iv3d.workstation.plugin.DisplaySettings;
import iv3d.workstation.plugin.PluginLogManager;
import iv3d.workstation.plugin.PluginManager;
import iv3d.workstation.plugin.domain.DefaultPluginLogManager;
import iv3d.workstation.plugin.domain.DefaultPluginManager;
import iv3d.workstation.ui.HasManagerContext;
import iv3d.workstation.ui.ManagerBootStrapper;
import iv3d.workstation.ui.UiManager;
import iv3d.workstation.ui.domian.DefaultUiManager;
import iv3d.workstation.ui.domian.DisplaySettingsBuilder;
import javafx.application.Application;
import javafx.stage.Stage;

public class WorkStationMain extends Application {

	private final UiManager uiManager;
	private final PathManager pathManager;
	private final PluginManager pluginManager;
	private final PluginLogManager pluginLogManager;

	public static void main(String[] args) {
		Application.launch(WorkStationMain.class, args);	
	}
	
	public WorkStationMain() { 
		ManagerBootStrapper bootStrapper = new ManagerBootStrapper() {
			@Override
			public HasManagerContext bootStrap(HasManagerContext hasManagerContext) {
				hasManagerContext.setPathManager(new ObjectProviderAdapter<PathManager>(pathManager));
				hasManagerContext.setPluginLogManager(new ObjectProviderAdapter<PluginLogManager>(pluginLogManager));
				hasManagerContext.setPluginManager(new ObjectProviderAdapter<PluginManager>(pluginManager));
				hasManagerContext.setUiManager(new ObjectProviderAdapter<UiManager>(uiManager));
				return hasManagerContext;
			}
		};
		
		Provider<PluginManager> pluginManagerProvider = new Provider<PluginManager>() {
			
			@Override
			public PluginManager get() {
				return pluginManager;
			}
		};
		
		uiManager = new DefaultUiManager(pluginManagerProvider);
		pathManager = new DefaultPathManager(pluginManagerProvider);
		pluginLogManager = new DefaultPluginLogManager();
		pluginManager = new DefaultPluginManager(bootStrapper);
	}

	@Override
	public void start(Stage stage) throws Exception {
	    BasicConfigurator.configure();
		configureJython();
		showDefaultDashboard(stage);
	}

	private void showDefaultDashboard(Stage stage) {
		
		DisplaySettings settings = new DisplaySettingsBuilder()
//			.setUndecorated()
//			.setTransparent()
		    .setFullscreen()
			.build();
		
		
//		String pluginId = "iv3d.workstation.dashboard.widget.sessioninfo";
		String pluginId = "iv3d.workstation.dashboard";
		UiTaskResult result = uiManager.display(pluginId, settings);
		if(result.hasError()) 
			throw new RuntimeException(result.getException());
	}

	private void configureJython() {
		JythonUtils.init();
		try {
			JythonUtils.addLibPath(pathManager.getPluginLocation());
			JythonUtils.addLibPath(pathManager.getScriptLocation());
		} catch (JythonException e) {
			throw new RuntimeException(e);
		}
	}
}
