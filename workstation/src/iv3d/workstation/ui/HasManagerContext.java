package iv3d.workstation.ui;

import com.iv3d.common.Provider;

import iv3d.workstation.paths.PathManager;
import iv3d.workstation.plugin.PluginLogManager;
import iv3d.workstation.plugin.PluginManager;

public interface HasManagerContext {

	public void setPathManager(Provider<PathManager> pathManager);

	public void setPluginLogManager(Provider<PluginLogManager> pluginLogManager);

	public void setPluginManager(Provider<PluginManager> pluginManager);

	public void setUiManager(Provider<UiManager> uiManager);

}
