package iv3d.workstation.ui;

public interface ManagerBootStrapper {
	public HasManagerContext bootStrap(HasManagerContext hasManagerContext);
}
