package iv3d.workstation.ui.domian;

import java.io.Serializable;

import iv3d.workstation.plugin.DisplaySettings;
import javafx.stage.Stage;

public final class DisplaySettingsBuilder {
	private boolean decorated = true;
	private String caption = "";
	private double width = -1;
	private double height = -1;
	private Stage stage;
	private boolean transparent=true;
	private boolean fullscreen=false;

	public DisplaySettingsBuilder() {
	}

	public final DisplaySettingsBuilder setUndecorated() {
		decorated = false;
		return this;
	}

	public final DisplaySettingsBuilder setSize(double width, double height) {
		this.width = width;
		this.height = height;
		return this;
	}

	public final DisplaySettingsBuilder setCaption(String caption) {
		this.caption = caption;
		return this;
	}

	public final DisplaySettingsBuilder setTransparent() {
		this.transparent=true;
		return this;
	}

	public final DisplaySettingsBuilder setOpaque() {
		this.transparent=false;
		return this;
	}

	public final DisplaySettingsBuilder setStage(Stage stage) {
		this.stage = stage;
		return this;
	}

	public final DisplaySettingsBuilder setFullscreen() {
		fullscreen = true;
		return this;
	}

	public final DisplaySettings build() {
		return new DisplaySettingsImpl(decorated, caption, width, height, transparent, stage, fullscreen);
	}
	
	class DisplaySettingsImpl implements DisplaySettings, Serializable {
		private static final long serialVersionUID = 1L;
	
		private final boolean decorated;
		private final String caption;
		private final double width;
		private final double height;
		private final boolean transparent;
		private final Stage stage;
		private final boolean fullscreen;


		public DisplaySettingsImpl(boolean decorated, String caption,
				double width, double height, boolean transparent, Stage stage, boolean fullscreen) {
			this.decorated = decorated;
			this.caption = caption;
			this.width = width;
			this.height = height;
			this.transparent = transparent;
			this.stage = stage;
			this.fullscreen = fullscreen;
		}

		public boolean isDecorated() {
			return decorated;
		}

		public String getCaption() {
			return caption;
		}

		public double getWidth() {
			return width;
		}

		public double getHeight() {
			return height;
		}
		
		public Stage getStage() {
			return stage;
		}

		@Override
		public boolean isTransparent() {
			return transparent;
		}

		@Override
		public boolean isFullscreen() {
			return fullscreen;
		}
	}
}
