package iv3d.workstation.ui.domian;

import com.iv3d.common.Color;
import com.iv3d.common.Provider;

import iv3d.ui.common.UiTaskResult;
import iv3d.ui.common.UiTaskResultBuilder;
import iv3d.workstation.domain.GlobalStyleSettings;
import iv3d.workstation.plugin.DisplaySettings;
import iv3d.workstation.plugin.Plugin;
import iv3d.workstation.plugin.PluginDisplayComponent;
import iv3d.workstation.plugin.PluginManager;
import iv3d.workstation.ui.UiManager;
import javafx.scene.Parent;

public class DefaultUiManager implements UiManager {
	private final Provider<PluginManager> pluginManagerProvider;

	public DefaultUiManager(Provider<PluginManager> pluginManagerProvider) {
		this.pluginManagerProvider = pluginManagerProvider;
	}

	@Override
	public UiTaskResult display(String pluginId, DisplaySettings settings) {
		PluginManager pluginManager = pluginManagerProvider.get();

		Plugin plugin = pluginManager.get(pluginId);
		if (plugin == null) {
			return getNoPluginError(pluginId);
		}

		try {
			PluginDisplayComponent displayComponent = plugin.getDisplayComponent();
			displayComponent.run(settings);
		} catch (Exception e) {
			String message = String.format(
					"Error occured while running view for plugin=%s", pluginId);
			return new UiTaskResultBuilder().setException(e)
					.setMessage(message).build();
		}
		return new UiTaskResultBuilder().build();

	}

	@Override
	public UiTaskResult getDisplayComponent(String pluginId) {
		PluginManager pluginManager = pluginManagerProvider.get();

		Plugin plugin = pluginManager.get(pluginId);
		if (plugin == null) {
			return getNoPluginError(pluginId);
		}

		try {
			PluginDisplayComponent viewComponent = plugin.getDisplayComponent();
			if (viewComponent == null)
				return new UiTaskResultBuilder().build();
			Parent parent = viewComponent.getParent();
			return new UiTaskResultBuilder().setValue(parent).build();
		} catch (Exception e) {
			String message = String.format(
					"Error occured while retreiving view for plugin=%s",
					pluginId);
			return new UiTaskResultBuilder().setException(e)
					.setMessage(message).build();
		}
	}

	@Override
	public GlobalStyleSettings getGlobalStyleSettings() {
		return new GlobalStyleSettingsImpl();
	}

	private UiTaskResult getNoPluginError(String pluginId) {
		String message = String.format("No plugin with id='%s' exists!",
				pluginId);
		return new UiTaskResultBuilder().setException(
				new NoPluginError(message)).setMessage(message).build();
	}

	public class NoPluginError extends RuntimeException {
		private static final long serialVersionUID = 1L;

		public NoPluginError(String message) {
			super(message);
		}
	}

	public final class GlobalStyleSettingsImpl implements GlobalStyleSettings {

		@Override
		public Color getBorderColor() {
			return new Color(120, 120, 122, 255);
		}

		@Override
		public Color getPanelFillColor() {
			return new Color(38, 38, 38, 255);
		}

		@Override
		public Color getPrimaryColor() {
			return new Color(72, 68, 67, 255);
		}
	}
}
