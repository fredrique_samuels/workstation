package iv3d.workstation.common;

import com.iv3d.common.Provider;

public class ObjectProviderAdapter<T> implements Provider<T> {

	private final T value;
	
	public ObjectProviderAdapter(T value) {
		this.value = value;
	}

	@Override
	public T get() {
		return value;
	}

}
