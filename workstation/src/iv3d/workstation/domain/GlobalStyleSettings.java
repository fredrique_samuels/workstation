package iv3d.workstation.domain;

import com.iv3d.common.Color;

public interface GlobalStyleSettings {
	public Color getPrimaryColor();
	public Color getPanelFillColor();
	public Color getBorderColor();
}
