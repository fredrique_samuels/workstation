package iv3d.workstation.paths.domain;

import java.io.File;
import java.net.URL;

import com.iv3d.common.Provider;
import com.iv3d.common.ResourceLoader;

import iv3d.workstation.paths.PathManager;
import iv3d.workstation.plugin.Plugin;
import iv3d.workstation.plugin.PluginManager;

public final class DefaultPathManager implements PathManager {

	private final URL pluginRootUrl;
	private final URL scriptLocation;
	private final Provider<PluginManager> pluginManagerProvider;

	public DefaultPathManager(Provider<PluginManager> pluginManagerProvider) {
		this.pluginManagerProvider = pluginManagerProvider;
		pluginRootUrl = ResourceLoader.getUrl("./appdata/plugins");
		scriptLocation = ResourceLoader.getUrl("./appdata/scripts");
	}
	
	@Override
	public URL getPluginLocation() {
		return pluginRootUrl;
	}
	
	public URL getPluginResource(String pluginId, String resourceId) {
		PluginManager pluginManager = pluginManagerProvider.get();
		Plugin plugin = pluginManager.get(pluginId);
		if(plugin==null) {
			return  null;
		}
		
		String resourceById = plugin.getResourceById(resourceId);
		if(resourceById==null)
			return null;
		File file = new File(getPluginResourcePath(pluginId), resourceById);
		return ResourceLoader.getUrl(file.getPath());
	}
	
	private String getPluginResourcePath(String pluginId) {
		return new File(pluginRootUrl.getPath(), pluginId).getPath();
	}

	@Override
	public URL getScriptLocation() {
		return scriptLocation;
	}
	
}
