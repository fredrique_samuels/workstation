package iv3d.workstation.paths;

import java.net.URL;

public interface PathManager {
	public URL getPluginLocation();
	public URL getScriptLocation();
}
