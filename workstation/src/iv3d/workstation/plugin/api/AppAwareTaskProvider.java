package iv3d.workstation.plugin.api;

import com.iv3d.common.Provider;

import iv3d.ui.common.UiTaskProvider;
import iv3d.workstation.paths.PathManager;
import iv3d.workstation.plugin.PluginLogManager;
import iv3d.workstation.plugin.PluginManager;
import iv3d.workstation.ui.HasManagerContext;
import iv3d.workstation.ui.UiManager;

public abstract class AppAwareTaskProvider implements UiTaskProvider, HasManagerContext{


	private Provider<PathManager> pathManager;
	private Provider<PluginLogManager> pluginLogManager;
	private Provider<PluginManager> pluginManager;
	private Provider<UiManager> uiManager;

	@Override
	public final void setPathManager(Provider<PathManager> pathManager) {
		this.pathManager = pathManager;
	}

	@Override
	public final void setPluginLogManager(Provider<PluginLogManager> pluginLogManager) {
		this.pluginLogManager = pluginLogManager;	
	}

	@Override
	public final void setPluginManager(Provider<PluginManager> pluginManager) {
		this.pluginManager = pluginManager;
	}

	@Override
	public final void setUiManager(Provider<UiManager> uiManager) {
		this.uiManager = uiManager;
	}

	public final PathManager getPathManager() {
		return pathManager.get();
	}

	public final PluginLogManager getPluginLogManager() {
		return pluginLogManager.get();
	}

	public final PluginManager getPluginManager() {
		return pluginManager.get();
	}

	public final UiManager getUiManager() {
		return uiManager.get();
	}
	
}
