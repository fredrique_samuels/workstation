package iv3d.workstation.plugin;

public interface Plugin {
	public PluginDisplayComponent getDisplayComponent();
	public String getResourceById(String resourceId);
}
