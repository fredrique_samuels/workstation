package iv3d.workstation.plugin;

public interface PluginLogManager {
	public PluginLogger get(String pluginId);
}
