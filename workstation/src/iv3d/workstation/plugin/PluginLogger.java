package iv3d.workstation.plugin;

public interface PluginLogger {
	public void info(Object o);
	public void warning(Object o);
	public void error(Object o);
}
