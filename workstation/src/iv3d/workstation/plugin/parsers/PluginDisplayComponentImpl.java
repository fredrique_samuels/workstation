package iv3d.workstation.plugin.parsers;

import java.io.File;

import iv3d.ui.javafx.controller.FxProfile;
import iv3d.ui.javafx.controller.FxProfileBuilder;
import iv3d.ui.javafx.controller.ModelPropertiesInitializer;
import iv3d.ui.javafx.controller.ViewInitializer;
import iv3d.workstation.beanloader.BeanConfig;
import iv3d.workstation.beanloader.BeanLoader;
import iv3d.workstation.plugin.DisplaySettings;
import iv3d.workstation.plugin.PluginDisplayComponent;
import iv3d.workstation.plugin.api.AppAwarePropertyListener;
import iv3d.workstation.plugin.api.AppAwareTaskProvider;
import iv3d.workstation.plugin.api.AppAwareViewInitializer;
import iv3d.workstation.plugin.domain.PluginConf;
import iv3d.workstation.plugin.domain.PluginSource;
import iv3d.workstation.ui.ManagerBootStrapper;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public final class PluginDisplayComponentImpl implements PluginDisplayComponent {

	private final FxProfile profile;
	
	public PluginDisplayComponentImpl(String path, PluginConf conf, ManagerBootStrapper bootStrapper) {
		String fxml = new File(path, conf.getFxml()).getPath();
		String jsFile = getControllerSource(conf);
		AppAwareTaskProvider provider = getTaskProvider(path, conf, bootStrapper);
		ModelPropertiesInitializer modelInitializer = getModelPropertiesInitializer(path, conf);
		ViewInitializer viewInitializer = getViewInitializer(path, conf, bootStrapper);
		AppAwarePropertyListener propertyChangeListener = getPropertyChangeListener(path, conf, bootStrapper);
		this.profile = new FxProfileBuilder()
			.setFxmlFile(fxml)
			.setJsFile(jsFile)
			.setTaskProvider(provider)
			.setModelPropertyInitializer(modelInitializer)
			.setViewInitializer(viewInitializer)
			.setPropertyChangeListener(propertyChangeListener)
			.build();
		propertyChangeListener.setNodeLookup(profile.getNodeLookup());
	}

	private ModelPropertiesInitializer getModelPropertiesInitializer(
			String path, PluginConf conf) {
		PluginSourceToBeanConfTransformer transformer = new PluginSourceToBeanConfTransformer(path);
		BeanConfig transform = transformer.transform(conf.getModelPropertiesInitializer());
		if(transform==null)
			return null;
		return (ModelPropertiesInitializer) new BeanLoader().load(transform, ModelPropertiesInitializer.class);
	}

	private ViewInitializer getViewInitializer(
			String path, PluginConf conf, ManagerBootStrapper bootStrapper) {
		PluginSourceToBeanConfTransformer transformer = new PluginSourceToBeanConfTransformer(path);
		BeanConfig transform = transformer.transform(conf.getViewInitializer());
		if(transform==null)
			return null;
		AppAwareViewInitializer viewInitializer = (AppAwareViewInitializer) new BeanLoader().load(transform, AppAwareViewInitializer.class);
		bootStrapper.bootStrap(viewInitializer);
		return viewInitializer;
	}

	private AppAwarePropertyListener getPropertyChangeListener(String path, PluginConf conf, ManagerBootStrapper bootStrapper) {
		PluginSourceToBeanConfTransformer transformer = new PluginSourceToBeanConfTransformer(path);
		BeanConfig transform = transformer.transform(conf.getPropertyChangeListener());
		if(transform==null)
			return null;
		AppAwarePropertyListener propertyListener = (AppAwarePropertyListener) new BeanLoader().load(transform, AppAwarePropertyListener.class);
		bootStrapper.bootStrap(propertyListener);
		return propertyListener;
	}

	private AppAwareTaskProvider getTaskProvider(String path, PluginConf conf, ManagerBootStrapper bootStrapper) {
		PluginSourceToBeanConfTransformer transformer = new PluginSourceToBeanConfTransformer(path);
		BeanConfig transform = transformer.transform(conf.getTaskFactory());
		if(transform==null)
			return null;
		AppAwareTaskProvider provider = (AppAwareTaskProvider) new BeanLoader().load(transform, AppAwareTaskProvider.class);
		bootStrapper.bootStrap(provider);
		return provider;
	}

	private String getControllerSource(PluginConf conf) {
		PluginSource controller = conf.getController();
		if(controller==null)
			return null;
		return controller.getSource();
	}
	
	@Override
	public void run(DisplaySettings settings) {
		Stage stage = settings.getStage();
		if(stage==null)
			stage = new Stage();
		
        stage.setTitle(settings.getCaption());
        double height = settings.getHeight();
		double width = settings.getWidth();
		
		Parent parent = profile.getParent();
		
		if(width<1 || height<1) {
			Scene scene = new Scene(parent);
			scene.setFill(null);
			stage.setScene(scene);
		} else {
			Scene scene = new Scene(parent, width, height);
			scene.setFill(null);
			stage.setScene(scene);
		}
		
        if(!settings.isDecorated()) {
        	stage.initStyle(StageStyle.UNDECORATED);
    	} else if(settings.isTransparent()) {
        	stage.initStyle(StageStyle.TRANSPARENT);
    	} else {
        	stage.initStyle(StageStyle.DECORATED);
    	}
        stage.setFullScreen(settings.isFullscreen());
        stage.show();
	}
	
	@Override
	public Parent getParent() {
		return profile.getParent();
	}
}
