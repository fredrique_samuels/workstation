package iv3d.workstation.plugin.parsers;

import com.iv3d.common.Transformer;

import iv3d.workstation.beanloader.BeanConfig;
import iv3d.workstation.plugin.domain.PluginSource;

public class PluginSourceToBeanConfTransformer implements Transformer<PluginSource, BeanConfig>{

	private final String path;

	public PluginSourceToBeanConfTransformer(String path) {
		this.path = path;
	}

	@Override
	public BeanConfig transform(PluginSource from) {
		if(from==null) {
			return null;
		}
		return new PluginSourceToBeanConfAdapter(path, from);
	}
	
	private class PluginSourceToBeanConfAdapter implements BeanConfig {

		private final String path;
		private final PluginSource from;

		public PluginSourceToBeanConfAdapter(String path, PluginSource from) {
			this.path = path;
			this.from = from;
		}

		@Override
		public String getBean() {
			return from.getBean();
		}

		@Override
		public String getLanguage() {
			return from.getLanguage();
		}

		@Override
		public String getPath() {
			return path;
		}

		@Override
		public String getSource() {
			return from.getSource();
		}
		
	}
}
