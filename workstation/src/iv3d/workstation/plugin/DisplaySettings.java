package iv3d.workstation.plugin;

import javafx.stage.Stage;

public interface DisplaySettings {
	public boolean isDecorated();
	public String getCaption();
	public double getWidth();
	public double getHeight();
	public Stage getStage();
	public boolean isTransparent();
	public boolean isFullscreen();
}
