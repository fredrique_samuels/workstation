package iv3d.workstation.plugin.domain;

import java.io.Serializable;

public class PluginSource implements Serializable {
	private static final long serialVersionUID = 1L;
	private String source;
	private String language;
	private String bean;

	public PluginSource() {
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getBean() {
		return bean;
	}

	public void setBean(String bean) {
		this.bean = bean;
	}

}
