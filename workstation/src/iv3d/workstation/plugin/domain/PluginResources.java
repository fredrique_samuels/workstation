package iv3d.workstation.plugin.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class PluginResources implements Serializable {
	private static final long serialVersionUID = 1L;
	private Map<String, PluginResource> map;
	
	public PluginResources() {
		map = new HashMap<String, PluginResource>();
	}
	
	public List<PluginResource> getRes() {
		return new ArrayList<PluginResource>(map.values());
	}
	
	public void setRes(List<PluginResource> res) {
		for(PluginResource r:res) {
			map.put(r.getId(), r);
		}
	}
	
	public final PluginResource get(String id) {
		return map.get(id);
	}
}
