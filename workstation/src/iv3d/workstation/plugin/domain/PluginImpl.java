package iv3d.workstation.plugin.domain;

import iv3d.workstation.plugin.Plugin;
import iv3d.workstation.plugin.PluginDisplayComponent;
import iv3d.workstation.plugin.parsers.PluginDisplayComponentImpl;
import iv3d.workstation.ui.ManagerBootStrapper;

import java.io.Serializable;
import java.net.URL;


public class PluginImpl implements Plugin, Serializable {
	private static final long serialVersionUID = 1L;
	
	private final URL pluginPath;
	private final PluginConf conf;
	private ManagerBootStrapper bootStrapper;

	public PluginImpl(URL pluginPath, PluginConf conf, ManagerBootStrapper bootStrapper) {
		this.conf = conf;
		this.pluginPath = pluginPath;
		this.bootStrapper = bootStrapper;
	}

	public PluginDisplayComponent getDisplayComponent() {		
		return new PluginDisplayComponentImpl(pluginPath.getPath(), conf, bootStrapper);
	}
	
	@Override
	public String getResourceById(String resourceId) {
		PluginResource pluginResource = conf.getResources().get(resourceId);
		if(pluginResource==null)
			return null;
		return pluginResource.getFile();
	}

}
