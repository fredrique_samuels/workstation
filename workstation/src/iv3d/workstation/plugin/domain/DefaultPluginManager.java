package iv3d.workstation.plugin.domain;

import java.io.File;
import java.io.FilenameFilter;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.iv3d.common.ResourceLoader;

import iv3d.workstation.plugin.Plugin;
import iv3d.workstation.plugin.PluginManager;
import iv3d.workstation.ui.ManagerBootStrapper;


public final class DefaultPluginManager implements PluginManager {
	
	private final URL pluginPath;
	private ManagerBootStrapper bootStrapper;
	private final Map<String, Plugin> plugins = new HashMap<String, Plugin>();
	
	public DefaultPluginManager(ManagerBootStrapper bootStrapper) {
		this.bootStrapper = bootStrapper;
		this.pluginPath = getPluginPath();
		loadPackages();
	}

	private URL getPluginPath() {
		String path = "appdata/plugins";
		URL url = ResourceLoader.getUrl(path);
		if(url==null)
			throw new InvalidPluginPathError(path);
		return url;
	}

	private void loadPackages() {
		File f = getPluginPathFile();
		FilenameFilter filenameFilter = new FilenameFilter() {
			
			@Override
			public boolean accept(File file, String arg1) {
				return arg1.endsWith(".plugin.xml");
			}
		};
		
		String[] list = f.list(filenameFilter);
		for (String pluginConfFile : list) {
			loadPackage(pluginConfFile);
		}
	}

	private void loadPackage(String pluginConfFile) {
		InputStream stream = ResourceLoader.read(new File(pluginPath.getPath(), pluginConfFile).getPath());
		JAXBContext jc;
		try {
			jc = JAXBContext.newInstance(PluginConf.class);
			Unmarshaller u = jc.createUnmarshaller();
			PluginConf conf = (PluginConf) u.unmarshal(stream);
			plugins.put(conf.getId(), new PluginImpl(pluginPath, conf, bootStrapper));
		} catch (JAXBException e) {
			throw new ConfigParsingError(e);
		}
	}
	
	private File getPluginPathFile() {
		String path = pluginPath.getPath();
		File f = new File(path);
		if(!f.exists()) {
			throw new InvalidPluginPathError(path);
		}
		return f;
	}
	
	public Plugin get(String pluginId) {
		return plugins.get(pluginId);
	}

	public class ConfigParsingError extends RuntimeException {
		private static final long serialVersionUID = 1L;

		public ConfigParsingError(JAXBException e) {
			super(e);
		}
	}
	
	public class InvalidPluginPathError extends RuntimeException {
		private static final long serialVersionUID = 1L;
		public InvalidPluginPathError(String path) {
			super(path);
		}
		public InvalidPluginPathError(Exception e, String path) {
			super(path, e);
		}
	}

}
