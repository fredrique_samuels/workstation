package iv3d.workstation.plugin.domain;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="plugin")
public class PluginConf {
	private String id;
	private String fxml;
	private PluginSource controller;
	private PluginSource taskFactory;
	private PluginSource modelPropertiesInitializer;
	private PluginSource viewInitializer;
	private PluginSource propertyChangeListener;
	private PluginResources resources;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getFxml() {
		return fxml;
	}
	public void setFxml(String fxml) {
		this.fxml = fxml;
	}
	public PluginSource getController() {
		return controller;
	}
	public void setController(PluginSource controller) {
		this.controller = controller;
	}
	public PluginSource getTaskFactory() {
		return taskFactory;
	}
	public void setTaskFactory(PluginSource taskfactory) {
		this.taskFactory = taskfactory;
	}
	public PluginSource getModelPropertiesInitializer() {
		return modelPropertiesInitializer;
	}
	public void setModelPropertiesInitializer(
			PluginSource modelPropertiesInitializer) {
		this.modelPropertiesInitializer = modelPropertiesInitializer;
	}
	public PluginSource getViewInitializer() {
		return viewInitializer;
	}
	public void setViewInitializer(PluginSource viewInitializer) {
		this.viewInitializer = viewInitializer;
	}
	public PluginResources getResources() {
		return resources;
	}
	public void setResources(PluginResources resources) {
		this.resources = resources;
	}
	public PluginSource getPropertyChangeListener() {
		return propertyChangeListener;
	}
	public void setPropertyChangeListener(PluginSource propertyChangeListener) {
		this.propertyChangeListener = propertyChangeListener;
	}
}
