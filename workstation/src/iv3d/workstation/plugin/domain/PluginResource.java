package iv3d.workstation.plugin.domain;

import java.io.Serializable;

public final class PluginResource implements Serializable {
	private static final long serialVersionUID = 1L;
	private String id;
	private String file;
	
	public PluginResource() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}
}
