package iv3d.workstation.plugin.domain;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import iv3d.workstation.plugin.PluginLogManager;
import iv3d.workstation.plugin.PluginLogger;

public class DefaultPluginLogManager implements PluginLogManager, Serializable {
	private static final long serialVersionUID = 1L;

	private Map<String, PluginLogger> loggers;
	
	public DefaultPluginLogManager() {
		loggers = new HashMap<String, PluginLogger>();
	}
	
	@Override
	public PluginLogger get(String pluginId) {
		if(!loggers.containsKey(pluginId)) {
			loggers.put(pluginId, new DefaultLogger(Logger.getLogger(String.format("PluginLog<%s>", pluginId))));
		}
		return loggers.get(pluginId);
	}
	
	private class DefaultLogger implements PluginLogger, Serializable {
		private static final long serialVersionUID = 1L;
		private final Logger logger;

		public DefaultLogger(Logger logger) {
			this.logger = logger;
		}

		@Override
		public void info(Object o) {
			logger.info(o);
		}

		@Override
		public void error(Object o) {
			logger.error(o);
		}

		@Override
		public void warning(Object o) {
			logger.warn(o);
		}
	}

}
