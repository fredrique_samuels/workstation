package iv3d.workstation.plugin;


public interface PluginManager {
	public Plugin get(String pluginId);
}
