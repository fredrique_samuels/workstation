package iv3d.workstation.plugin;

import javafx.scene.Parent;

public interface PluginDisplayComponent {
	public void run(DisplaySettings settings);
	public Parent getParent();
}
