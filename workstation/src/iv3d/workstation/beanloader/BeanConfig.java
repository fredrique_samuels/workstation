package iv3d.workstation.beanloader;

public interface BeanConfig {
	public String getSource();
	public String getLanguage();
	public String getBean();
	public String getPath();

}
