package iv3d.workstation.beanloader;

import java.util.HashMap;
import java.util.Map;

public final class BeanLoader {
	public static final String JYTHON_BEAN = "jython";
	public static final String JAVA_BEAN = "java";
	
	private final Map<String, BeanLoaderStrategy> sourceParsers;
	
	public BeanLoader() {
		sourceParsers = new HashMap<String, BeanLoaderStrategy>();
		sourceParsers.put(JYTHON_BEAN, new JythonBeanLoaderStrategy());
		sourceParsers.put(JAVA_BEAN, new JavaBeanLoaderStrategy());
	}

	public final void addStategy(String langauge, BeanLoaderStrategy loader) {
		sourceParsers.put(langauge, loader);
	}
	
	public final Object load(BeanConfig beanConfig, Class<?> clazz) {
		String language = beanConfig.getLanguage();
		if(language==null) {
			language = JAVA_BEAN;
		}
		
		BeanLoaderStrategy loader = sourceParsers.get(language);
		if(loader==null) {
			throw new LanguageNotSupportedError(language);
		}
		
		return loader.load(beanConfig, clazz);
	}

	public final class LanguageNotSupportedError extends RuntimeException {
		private static final long serialVersionUID = 1L;

		public LanguageNotSupportedError(String language) {
			super(String.format("The language %s is not suppported!", language));
		} 		
	}
}
