package iv3d.workstation.beanloader;

import java.net.URL;

import com.iv3d.common.ResourceLoader;

import iv3d.workstation.jython.JythonUtils;
import iv3d.workstation.jython.JythonUtils.JythonException;

public class JythonBeanLoaderStrategy implements
		BeanLoaderStrategy {

	@Override
	public <T> T load(BeanConfig beanConfig, Class<T> clazz) {
		String path = beanConfig.getPath();
		if(path==null)
			throw new NoModulePathSetError();
		
		URL url = ResourceLoader.getUrl(path);
		if(url==null)
			throw new CannotResolvePathError(path);
		
		try {
			JythonUtils.addLibPath(url.getFile());
			return JythonUtils.newBean(beanConfig.getBean(), clazz);
		} catch (JythonException e) {
			throw new JythonError(e);
		}
	}
	
	public final class NoModulePathSetError extends RuntimeException {
		private static final long serialVersionUID = 1L;
		public NoModulePathSetError() {
			super();
		}
	}

	public final class CannotResolvePathError extends RuntimeException {
		private static final long serialVersionUID = 1L;
		public CannotResolvePathError(String path) {
			super(path);
		}
	}

	public final class JythonError extends RuntimeException {
		private static final long serialVersionUID = 1L;
		public JythonError(JythonException e) {
			super(e);
		}
	}
}
