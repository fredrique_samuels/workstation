package iv3d.workstation.beanloader;


public interface BeanLoaderStrategy {
	public <T> T load(BeanConfig beanConfig, Class<T> clazz);
}
