package iv3d.workstation.beanloader;


public class JavaBeanLoaderStrategy implements BeanLoaderStrategy {

	@Override
	public <T> T load(BeanConfig beanConfig, Class<T> clazz) {
		try {
			Class<?> type = ClassLoader.getSystemClassLoader().loadClass(beanConfig.getBean());
			Object newInstance = type.newInstance();
			return clazz.cast(newInstance);
		} catch (ClassNotFoundException e) {
			throw new ClassNotFoundError(e);
		} catch (InstantiationException e) {
			throw new InitError(e);
		} catch (IllegalAccessException e) {
			throw new AccessError(e);
		}
	}
	
	public class AccessError extends RuntimeException {
		private static final long serialVersionUID = 1L;

		public AccessError(IllegalAccessException e) {
			super(e);
		}
	}
	
	public class InitError extends RuntimeException {
		private static final long serialVersionUID = 1L;

		public InitError(InstantiationException e) {
			super(e);
		}
	}
	
	public class ClassNotFoundError extends RuntimeException {
		private static final long serialVersionUID = 1L;

		public ClassNotFoundError(ClassNotFoundException e) {
			super(e);
		}
	}

}
