from iv3d.ui.common import MappedTaskProvider
from iv3d.ui.common import UiTask
from iv3d.ui.common import UiTaskContext
from iv3d.ui.common import UiTaskResult
from iv3d.ui.common import UiTaskResultBuilder
from iv3d.ui.common import UiTaskStatus
from iv3d.ui.common import UiTaskProvider

from iv3d.ui.javafx.controller import ModelPropertiesInitializer
from iv3d.ui.javafx.controller import ViewInitializer
from iv3d.ui.javafx.controller import PropertyChangeListener 

from iv3d.workstation.plugin.api import AppAwareViewInitializer
from iv3d.workstation.plugin.api import AppAwareTaskProvider
from iv3d.workstation.plugin.api import AppAwarePropertyListener

from javafx.scene.image import Image
from javafx.utils import CssUtil


PLUGIN_ID = "iv3d.workstation.dashboard.widget.sessioninfo"


STYLE_SESSION_INFO = """
    -fx-scale-shape:false;
    -fx-position-shape:false;
    -fx-background-color: %(background_color)s;
    -fx-border-color: %(border_color)s;
    -fx-border-width:2;
    -fx-shape:"M 5,50 L 75,50 75,11 81,2 280,2 289,12 289,50 280,60 240,60 235,56 93,56 52,50 Z"; """

def getSessionInfoStyle(uiManager):
    styleSettings = uiManager.getGlobalStyleSettings()
    pc = styleSettings.getPrimaryColor()
    bc = styleSettings.getBorderColor()
    
    colors = {'background_color':pc.toHexRgba(),
              'border_color':CssUtil.derive(bc.toHexRgb(), 25)}
    
    return STYLE_SESSION_INFO % colors
   
class ModelSetup(ModelPropertiesInitializer):
    
    def __init__(self, *args):
        ModelPropertiesInitializer.__init__(self)
        
    def setup(self, properties):
        print self
        pass
        
class ViewSetup(AppAwareViewInitializer):
    
    def __init__(self, *args):
        AppAwareViewInitializer.__init__(self)
    def setup(self, root, nodeMap):
        print "Loading system logo."
        logger = self.getPluginLogManager().get(PLUGIN_ID)
        self.setSystemLogo(nodeMap, logger)
        self.setDefaultUserImage(nodeMap, logger)
        self.setBackgroundStyle(nodeMap, logger)
    def setBackgroundStyle(self, nodeMap, logger):
        node = nodeMap.get("backgroundRegion")
        if not node:
            logger.warning("Node backgroundRegion could not be found!!")
            return
        node.setStyle(getSessionInfoStyle(self.getUiManager()));
    def setSystemLogo(self, nodeMap, logger):
        logger.info("Loading system logo.")
        node = nodeMap.get("systemLogo")
        if not node:
            logger.warning("Node systemLogo could not be found!!")
            return
            
        pathManager = self.getPathManager();
        imageUrl = pathManager.getPluginResource(PLUGIN_ID, "shield_logo");
        if not imageUrl:
            logger.warning("No System logo set!!")
            return
        
        image = Image(imageUrl.toExternalForm())
        node.setImage(image);
    def setDefaultUserImage(self, nodeMap, logger):
        logger.info("Loading default user image.")
        node = nodeMap.get("userIcon")
        if not node:
            logger.warning("Node userIcon could not be found!!")
            return
            
        pathManager = self.getPathManager()
        imageUrl = pathManager.getPluginResource(PLUGIN_ID, "default_user_profile_pic");
        if not imageUrl:
            logger.warning("No User Icon set!!")
            return
        
        image = Image(imageUrl.toExternalForm())
        node.setImage(image);

class PropertyChangeListenerImpl(AppAwarePropertyListener):
    
    def __init__(self, *args):
        AppAwarePropertyListener.__init__(self)
    def onChange(self, *args):
        print args

	