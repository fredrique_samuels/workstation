// Imports
importClass(java.lang.System);

//=================================
var __top_component = null;
var __left_component = null;
var __bottom_component = null;
var __right_component = null;

var __top_left_component = null;
var __top_right_component = null;
var __bottom_left_component = null;
var __bottom_right_component = null;

// UI Model
var __viewModel = {
	"top_size":50,
	"left_size":50,
	"bottom_size":50,
	"right_size":50
}

//==================================
// Framework functions

function __get_view__() {
	return __viewModel;
}

function __set_view__(view) {
	__viewModel = view
}	

function __update_view__() {
	__set_view__(__get_view__())
}

function __init__() {
	loadTopLeft();
}

//==================================
// Logic functions

function loadTopLeft() {
	controller.buildTask("loadTopLeft")
		.setParam('root', dashboard_root)
		.setParam('styleLayer0Row0', styleLayer0Row0)
		.setParam('componentLayerRow0', componentLayerRow0)
		.execute();
}

function validateResult(result) {
	var component = result.getValue();
	var noError = !result.hasError();
	return noError && component;
}

function setTopLeft(result) {
	if(validateResult(result)) {
		dashboard_root.add(result.getValue(), 0, 0);
	}
}
