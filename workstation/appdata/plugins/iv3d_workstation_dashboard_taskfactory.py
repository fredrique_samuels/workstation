from iv3d.ui.common import MappedTaskProvider
from iv3d.ui.common import UiTask
from iv3d.ui.common import UiTaskContext
from iv3d.ui.common import UiTaskResult
from iv3d.ui.common import UiTaskResultBuilder
from iv3d.ui.common import UiTaskStatus
from iv3d.ui.common import UiTaskProvider

from iv3d.workstation.plugin.api import AppAwareViewInitializer
from iv3d.workstation.plugin.api import AppAwareTaskProvider
from iv3d.workstation.plugin.api import AppAwarePropertyListener

from iv3d.ui.javafx.controller import FxRunner
from javafx.utils import CssUtil

from javafx.geometry import HPos
from javafx.geometry import VPos
from javafx.scene.layout import GridPane
from javafx.scene.layout import Priority
from javafx.scene.layout import Region
from javafx.scene.control import Button
from javafx.scene import Node

from java.lang import Runnable
from java.lang import System

PLUGIN_ID = "iv3d.workstation.dashboard"

class PluginState:
    def __init__(self):
        self.display_components = {}
        self.screen_width = 600
        self.screen_height = 400
    def process_root_pc(self, prop, val):
        if 'width'==prop:
            self.screen_width = val
        elif 'height'==prop:
            self.screen_height = val
        
PLUGIN_STATE = PluginState()        
STYLE_NODE_ID = "styleLayer0"
STYLE_SESSION_INFO = """
    -fx-scale-shape:false;
    -fx-position-shape:false;
    -fx-background-color: %(background_color)s;
    -fx-border-color: %(border_color)s;
    -fx-border-width:1;
    -fx-shape:"%(shape)s"; """

def buildPolygon(*args):
    r = "M %d,%d L " % args[0]
    for index in range(1, len(args)):
        r+= "%d,%d " %(args[index])
    r+="Z"
    return r
    
def getSessionInfoStyle(uiManager):
    # see dash_bg_mask1.png
    
    # region vaiables
    width = PLUGIN_STATE.screen_width
    grid_y_0 = 120
    grid_x_0 = 313
    grid_x_1 = 540
    top_inset = 2
    left_inset = 10
    right_inset = 10
    
    # item variables
    item_slope = 5
    item_height = 15
    item_left_inset = 10
    item_right_inset = 10
    item_top_inset = 10
    
    # laf variables
    corner_slope = 20
    right_bar_width = 10
    
    # derived variables
    right_x = width-right_inset
    left_x = left_inset
    left_bottom_y = int(grid_y_0 * .5)
    top_y = top_inset
    item_top_y = item_top_inset
    item_bottom_y = item_top_y + item_height
    right_inside_x = right_x-right_bar_width
    sq_slope = left_bottom_y-item_bottom_y
    bottom_y = grid_y_0
    item_bottom_slope = item_height-item_slope
    
    a = (left_x+corner_slope, top_y)
    b = (right_x-corner_slope,top_y)
    c = (right_x,top_y+corner_slope)
    d = (right_x, bottom_y)
    e = (right_inside_x+5,bottom_y)
    f = (right_inside_x, bottom_y-5)
    g = (right_inside_x, item_bottom_y+corner_slope)
    h = (right_inside_x-corner_slope, item_bottom_y)
    k = (grid_x_1, item_bottom_y)
    l = (grid_x_1-item_bottom_slope, item_bottom_y-item_bottom_slope)
    m = (grid_x_1-item_bottom_slope-item_right_inset, item_bottom_y-item_bottom_slope)
    n = (grid_x_1-item_height-item_right_inset, item_top_y)
    o = (grid_x_0+sq_slope+item_left_inset+item_height, item_top_y)
    p = (grid_x_0+sq_slope+item_left_inset, item_bottom_y)
    q = (grid_x_0+sq_slope, item_bottom_y)
    s = (grid_x_0, left_bottom_y)
    t = (left_x, left_bottom_y)
    u = (left_x,top_y+corner_slope)
    
    poly = buildPolygon(a,b,c,d,e,f,g,h,k,l,m,n,o,p,q,s,t,u)
    
    styleSettings = uiManager.getGlobalStyleSettings()
    pc = styleSettings.getPrimaryColor()
    bc = styleSettings.getBorderColor()

    colors = {'background_color':pc.toHexRgba(),
              'border_color':CssUtil.derive(bc.toHexRgb(), 25),
              'shape':poly}

    return STYLE_SESSION_INFO % colors
    

def updateTopStyle(uiManager, node):
    style = getSessionInfoStyle(uiManager)
    class R(FxRunner):
        def run(self):
            node.setStyle(style)
    R().execute()
       
class ComponentSource:
    def get(self):
        pass
        
class PluginDisplaySource(ComponentSource):
    def __init__(self, plugin):
        self.plugin=plugin
    def get(self):
        UiTaskResultBuilder().configureAsError()
     

class ViewSetup(AppAwareViewInitializer):
    
    def __init__(self, *args):
        AppAwareViewInitializer.__init__(self)
    def setup(self, root, nodeMap):
        logger = self.getPluginLogManager().get(PLUGIN_ID)
        node = nodeMap.get(STYLE_NODE_ID)
        if not node:
            logger.warning("Node styleLayer0 could not be found!!")
            return
        updateTopStyle(self.getUiManager(), node)
        
class ComponentTask(UiTask):
    
    def __init__(self, uiManager, logger):
        UiTask.__init__(self)
        self.uiManager = uiManager
        self.logger = logger
        self.targetPlugin = "iv3d.workstation.dashboard.widget.sessioninfo"
        self.componentLayerId = 'componentLayerRow0'
        self.gridLocation = (0, 0)
    
    def run(self, context):
        targetGrid = context.getParam(self.componentLayerId, Node)
        
        result = self.uiManager.getDisplayComponent(self.targetPlugin)
        if result.hasError():
            self.logger.error(result.getError())
            return result
            
        sessionInfoDisplay = result.getValue()
        if not sessionInfoDisplay:
            message = "Plugin " + self.targetPlugin + " does not have a display component."
            self.logger.warning(message)
            return UiTaskResultBuilder().setMessage(message).build()
            
        x, y = self.gridLocation
        class R(FxRunner):
            def run(self):
                targetGrid.add(sessionInfoDisplay, x, y)
        R().execute()
     
        return UiTaskResultBuilder().setValue(sessionInfoDisplay).build()
        
class PropertyChangeListenerImpl(AppAwarePropertyListener):
    
    def __init__(self, *args):
        AppAwarePropertyListener.__init__(self)
    def onChange(self, *args):
        nodeId, prop, oldVal, newVal = args
        isRoot = self.lookup("dashboard_root")==nodeId
        if isRoot:
            pass
            PLUGIN_STATE.process_root_pc(prop, newVal)
            updateTopStyle(self.getUiManager(), self.lookup("dashboard_root"))
        print args
    
class TaskProvider (AppAwareTaskProvider):

    def __init__(self, *args):
        AppAwareTaskProvider.__init__(self)
        self.tasks = {"loadTopLeft":self.createTopLeft}
    
    def get(self, taskName):
        if taskName in self.tasks.keys():
            return self.tasks[taskName]()
            
    def __getLogger(self):
        return self.getPluginLogManager().get(PLUGIN_ID)
            
    def createTopLeft(self):
        return ComponentTask(self.getUiManager(), self.__getLogger())