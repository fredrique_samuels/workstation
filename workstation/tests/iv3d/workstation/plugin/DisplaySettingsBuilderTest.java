package iv3d.workstation.plugin;

import iv3d.workstation.plugin.DisplaySettings;
import iv3d.workstation.ui.domian.DisplaySettingsBuilder;
import junit.framework.TestCase;

public class DisplaySettingsBuilderTest extends TestCase {
	private DisplaySettingsBuilder instance;

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		instance = new DisplaySettingsBuilder();
	}
	
	public void testDefaults() {
		DisplaySettings build = instance.build();
		assertEquals("", build.getCaption());
		assertEquals(true, build.isDecorated());
		assertEquals(-1.0, build.getWidth(), .001);
		assertEquals(-1.0, build.getHeight(),.001);
	}
	
	public void testUserValues() {
		DisplaySettings build = instance
			.setCaption("caption")
			.setSize(200, 10)
			.setUndecorated()
			.build();
		assertEquals("caption", build.getCaption());
		assertEquals(false, build.isDecorated());
		assertEquals(200.0, build.getWidth(), .001);
		assertEquals(10.0, build.getHeight(), .001);
	}
}
