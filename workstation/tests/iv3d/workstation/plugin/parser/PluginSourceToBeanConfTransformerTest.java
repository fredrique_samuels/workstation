package iv3d.workstation.plugin.parser;

import iv3d.workstation.beanloader.BeanConfig;
import iv3d.workstation.plugin.domain.PluginConf;
import iv3d.workstation.plugin.domain.PluginSource;
import iv3d.workstation.plugin.parsers.PluginSourceToBeanConfTransformer;
import junit.framework.TestCase;

public class PluginSourceToBeanConfTransformerTest extends TestCase {
	
	private PluginSourceToBeanConfTransformer instance;

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		instance = new PluginSourceToBeanConfTransformer("/home"); 
	}
	
	public void testNullTransform() {
		assertNull(instance.transform(null));
	}
	
	public void testTransform() {
		PluginSource from = new PluginSource();
		from.setBean("Some.bean");
		from.setLanguage("jython");
		from.setSource("source.py");
		
		BeanConfig to = instance.transform(from);
		assertNotNull(to);
		assertEquals("Some.bean", to.getBean());
		assertEquals("jython", to.getLanguage());
		assertEquals("source.py", to.getSource());
		assertEquals("/home", to.getPath());
	}
}
