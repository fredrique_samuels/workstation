package iv3d.workstation.plugin.beanloader;

import iv3d.workstation.beanloader.BeanConfig;
import iv3d.workstation.beanloader.BeanLoader;
import iv3d.workstation.jython.JythonUtils;
import junit.framework.TestCase;

public class BeanLoaderTest extends TestCase {
	
	private BeanLoader instance;

	static  {
		JythonUtils.init();
	}
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		instance = new BeanLoader();
	}
	
	public void testNotASupportedLanguage() {
		String source = null;
		String language="does not exists";
		String bean=null;
		String path=null;
		BeanConfig beanConfig = new TestConfig(source, language, bean, path);
		
		try {
			instance.load(beanConfig , Message.class);
			fail();
		} catch (BeanLoader.LanguageNotSupportedError e) {
		}
	}
	
	public void testJavaBeanLoader() {
		String source = null;
		String language="java";
		String bean="iv3d.workstation.plugin.beanloader.JavaMessage";
		String path=null;
		BeanConfig beanConfig = new TestConfig(source, language, bean, path);
		
		Message object = (Message) instance.load(beanConfig , Message.class);
		assertNotNull(object);
		assertEquals("java message", object.get());
	}
	
	public void testJythonBeanLoader() {
		String source = "test.py";
		String language="jython";
		String bean="test.JythonMessage";
		String path="./tests/iv3d/workstation/plugin/beanloader";
		BeanConfig beanConfig = new TestConfig(source, language, bean, path);
		
		Message object = (Message) instance.load(beanConfig , Message.class);
		assertNotNull(object);
		assertEquals("jython message", object.get());
	}
	
	class TestConfig implements BeanConfig {
		private String source;
		private String language;
		private String bean;
		private String path;
		
		public TestConfig(String source, String language, String bean,
				String path) {
			super();
			this.source = source;
			this.language = language;
			this.bean = bean;
			this.path = path;
		}

		public String getSource() {
			return source;
		}

		public String getLanguage() {
			return language;
		}

		public String getBean() {
			return bean;
		}

		public String getPath() {
			return path;
		}
	}
}
